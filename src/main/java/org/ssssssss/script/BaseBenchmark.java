package org.ssssssss.script;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.util.FileUtils;

import javax.script.SimpleBindings;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

@Fork(2)
@Warmup(iterations = 5, time = 5)
@Measurement(iterations = 5, time = 10)
@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@State(Scope.Benchmark)
public class BaseBenchmark {

	public BaseBenchmark() {
		try {
			this.setup();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void setup() throws Exception {

	}

	protected SimpleBindings getBindings() {
		SimpleBindings bindings = new SimpleBindings();
		bindings.put("sum", 0L);
		bindings.put("step", 1);
		return bindings;
	}

	protected String readScript(String path) throws IOException {
		InputStream is = Magic.class.getResourceAsStream(path);
		return String.join("\r\n", FileUtils.readAllLines(is));
	}
}
