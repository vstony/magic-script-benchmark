package org.ssssssss.script;

import jdk.nashorn.api.scripting.NashornScriptEngine;
import jdk.nashorn.api.scripting.NashornScriptEngineFactory;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Setup;

import javax.script.CompiledScript;
import javax.script.ScriptException;

public class Nashorn extends BaseBenchmark {

	private CompiledScript compiledScript;

	@Setup
	@Override
	public void setup() throws Exception {
		NashornScriptEngine engine = (NashornScriptEngine) new NashornScriptEngineFactory().getScriptEngine();
		this.compiledScript = engine.compile(readScript("/nashorn.js"));
	}

	@Benchmark
	public void benchmark() throws ScriptException {
		this.compiledScript.eval(getBindings());
	}
}
